import './App.css';
import NavbarComponent from './components/Navbar';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Sucess from './Pages/Success';
import Cancel from './Pages/Cancel';
import Store from './Pages/Store';

function App() {
  return (
    <Container>
      <NavbarComponent> </NavbarComponent>

      <BrowserRouter>
        <Routes>
          <Route index element={<Store />} />
          <Route path='cancel' element={<Cancel />} />
          <Route path='sucess' element={<Sucess />} />
        </Routes>
      </BrowserRouter>
    </Container>
  );
}

export default App;
