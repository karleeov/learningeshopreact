const productsArray = [
  { id: '1', title: 'coffee', price: '4,99' },
  { id: '3', title: 'coffee', price: '4,99' },
  { id: '2', title: 'coffee', price: '4,99' },
];

function getProductData(id) {
  let productData = productsArray.find((product) => product.id === id);

  if (productData == undefined) {
    console.log('prod' + id);
    return undefined;
  }
  return productData;
}

export { productsArray, getProductData };
