import { Button, Container, Navbar, Modal } from 'react-bootstrap';
import { useState } from 'react';
function NavbarComponent() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Navbar expand='sm'>
        <Navbar.Brand href='/'>E store</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className='justify-content-end'>
          <Button onClick={handleShow}>Cart 0 item</Button>
        </Navbar.Collapse>
      </Navbar>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <h1>This is modal</h1>
        </Modal.Header>
      </Modal>
    </>
  );
}

export default NavbarComponent;
